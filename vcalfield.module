<?php

/**
 * @file
 * Defines a field type for vcal download.
 */

function vcalfield_field_info() {
  return array(
    'vcal' => array('label' => 'VCal')
  );
}

function vcalfield_field($op, &$node, $field, &$items, $teaser, $page){
  switch ($op) {
    case 'view':

    case 'validate':
      return 'validate';
  }
}

function vcalfield_field_formatter_info() {
  return array(
    'default' => array(
      'label' => 'VCalendar',
      'field types' => array('vcal'),
    )
  );
}

function vcalfield_field_formatter($field, $item, $formatter, $node) {
  return '';
}

function vcalfield_widget_info() {
  return array(
    'vcalendar' => array(
      'label' => 'VCalendar',
      'field types' => array('vcal'),
    ),
  );
}

function vcalfield_widget($op, &$node, $field, &$items) {
  switch ($op) {
    case 'prepare form values':
      if ($field['multiple']) {
        $items_transposed = content_transpose_array_rows_cols($items);
        $items['default nids'] = $items_transposed['nid'];
      }
      else {
        $items['default nids'] = array($items['nid']);
      }
      break;

    case 'form':
      $form = array();

      $form[$field['field_name']] = array('#tree' => TRUE);
      return $form;

    case 'process form values':
      if ($field['multiple']) {
        $items = content_transpose_array_rows_cols(array('nid' => $items['nids']));
      }
      else {
        $items['nid'] = is_array($items['nids']) ? reset($items['nids']) : $items['nids'];
      }
      break;
  }
}

function vcalfield_widget_settings($op, $widget) {
  switch ($op) {
    case 'form':
      $form = array();
      $options1 = _vcalfield_cck_fields();
      $options2 = _vcalfield_node_fields();

      $options = array_merge( $options1, $options2);

      $form['summary'] = array(
        '#type' => 'select',
        '#options' => $options,
        '#default_value' => $widget['summary'],
        '#title' => 'Summary',
      );
      $form['location'] = array(
        '#type' => 'select',
        '#options' => $options,
        '#default_value' => $widget['location'],
        '#title' => 'Location',
      );
      $form['descriptions'] = array(
        '#type' => 'select',
        '#options' => $options,
        '#default_value' => $widget['descriptions'],
        '#title' => 'Description',
      );
      $form['start'] = array(
        '#type' => 'select',
        '#options' => $options,
        '#default_value' => $widget['start'],
        '#title' => 'Start',
      );
      $form['end'] = array(
        '#type' => 'select',
        '#options' => $options,
        '#default_value' => $widget['end'],
        '#title' => 'End',
      );
      return $form;
    case 'validate':
      break;
    case 'save':
      return array('summary', 'location', 'descriptions', 'start', 'end');
  }
}

/**
 * Help functions
 *
 */

/**
 * Get the Node fields
 */
function _vcalfield_node_fields() {

  $options = array();
      $options['%title'] = 'VCal:'.t('Node Title');
      $options['%teaser'] = 'VCal:'.t('Node Teaser');
      $options['%body'] = 'VCal:'.t('Node Body');
      $options['%nodelink'] = 'VCal:'.t('Node Link');
  return $options;
}

/**
 * Get the CCK fields
 */
function _vcalfield_cck_fields() {
  $output = '';
  $fields = content_fields();
  $form = array();

  $options = array();
  foreach ($fields as $field) {
    $options[$field['field_name']] = t($field['widget']['label']) .' ('. $field['field_name'] .')';
  }
  return $options;
}

function vcalfield_menu($may_cache) {

  $items = array();

     $items[] = array(
      'path'     => 'vcalfield/' . arg(1) ,
      'title'    => t('Vcal Fields'),
      'description' => t('Show VCal fields.'),
      'access'   => user_access('myevents subscribe to event'),
      'callback' => 'vcalfield_file',
      'type' => 'MENU_CALLBACK'
    );

return $items;

}

function vcalfield_file() {
  global $user;
  global $base_url;

  $nid = arg(1);
  $node = node_load(array('nid' => $nid));

  $event_url = $base_url . $url_helper . '/node/' . $nid;	// TODO: only works with clean URLs
  $vcal_url = $base_url . $url_helper . '/vcalfield/' . $nid; // TODO: only works with clean URLs

  $Filename = "DrupalEvent" . $nid . ".vcs";
  #header("Content-Type: text/x-vCalendar");
  #header("Content-Disposition: inline; filename=$Filename");

  $instance_info = db_fetch_array(db_query("SELECT widget_settings FROM {node_field_instance} WHERE widget_type = '%s' AND type_name = '%s'", 'vcalendar', $node->type));
  $fields = unserialize($instance_info['widget_settings']);

  $defaultvars = array('%title', '%teaser', '%nodelink', '%body');

  $rfields = array();

  foreach ($fields as $field => $key){
    switch ($key) {
      case '%title':
        $rfields[$field] = $node->title;
        break;
      case '%teaser':
        $rfields[$field] = $node->teaser;
        break;
      case '%body':
        $rfields[$field] = $node->body;
        break;
      case '%nodelink':
        $rfields[$field] = url('node/'.$node->nid, NULL, NULL, TRUE);
        break;
      default:
        if ($key){
          $fd = $node->$key;
          if ($field == 'end'){
            if ($fields['end'] == $fields['start']){
              $rfields[$field] = $fd['0']['value2'];
            }
          } else {
            $rfields[$field] = $fd['0']['value'];
          }
        }
        break;
    }
  }

  #$Tzone     = ($user->uid ? intval($user->timezone / 3600) : -5);    // TODO: format tzone to 2 digits?
  $nodeCreated = format_date($node->created, 'custom', "Ymd") . "T" . format_date($node->created, 'custom', "Hi00");
  $nodeChanged = format_date($node->changed, 'custom', "Ymd") . "T" . format_date($node->changed, 'custom', "Hi00");

  $summary = vcalfield_html2text($rfields['summary']);
  $location = vcalfield_html2text($rfields['location']);
  $description = vcalfield_html2text($rfields['descriptions']);
  $start = vcalfield_html2text($rfields['start']);
  $end = vcalfield_html2text($rfields['end']);

  $output = "BEGIN:VCALENDAR
  VERSION:2.0
  METHOD:PUBLISH
  PRODID:Calendar
  TZ:$Tzone
  BEGIN:VEVENT
  DCREATED:$nodeCreated
  SUMMARY: $summary
  LOCATION: $location
  URL: $location
  UID: $vcal_url
  DTSTAMP: $nodeCreated
  DESCRIPTION;ENCODING=QUOTED-PRINTABLE: $description
  DTSTART: $start
  DTEND: $end
  LAST-MODIFIED:$nodeChanged
  END:VEVENT
  END:VCALENDAR
  ";

  $output = preg_replace("/\n\s/", "\n", $output);

  print $output;
  exit;
}

function vcalfield_html2text($htmltext) {
  $search = array (
    "'<script[^>]*?>.*?</script>'si",  // Strip out javascript
    "'<\/p>'si",                       // Change para's to double-newline
    "'<br[\s]*[\/]*>'si",              // Change BR's to newline
    "'<[\/\!]*?[^<>]*?>'si",          // Strip out HTML tags
    "'([\r\n])[\s]+'",                // Strip out white space
    "'&(quot|#34);'i",                // Replace HTML entities
    "'&(amp|#38);'i",
    "'&(lt|#60);'i",
    "'&(gt|#62);'i",
    "'&(nbsp|#160);'i",
    "'&(iexcl|#161);'i",
    "'&(cent|#162);'i",
    "'&(pound|#163);'i",
    "'&(copy|#169);'i",
    "'&#(\d+);'e");                    // evaluate as php

  $replace = array ("","\n\n","\n","","\\1","\"","&","<",">"," ",chr(161),chr(162),chr(163),chr(169),     "chr(\\1)");

  $text = preg_replace($search, $replace, $htmltext);
  return $text;
}